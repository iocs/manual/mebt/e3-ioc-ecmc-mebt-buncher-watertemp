#-d /**
#-d   \brief hardware script for EL3202-0010-Sensor-chX_S+S_RegelTechnik_HTF50_PT100
#-d   \details Parmetrization of EL3201 for S+S Regeltechnik HFT50 PT100 sensor
#-d   \author Anders Sandstroem
#-d   \file
#-    \pre Environment variables needed:
#-    \pre ECMC_EC_SDO_INDEX "0x80n0" where n is channel 0..3
#-    \pre ECMC_EC_SLAVE_NUM = slave number
#-d */

############################################################
############# Parmetrization of EL32X4 for S+S Regeltechnik HFT50 PT100 sensor
#- Type: PT100
#- Range: -35 to 105 degC
#- Connectiontype : 2 wire
#- Cable length: 1.5m
#- Cable area: 0.25mm²


#- Presentation:
#- 0 = Signed (Default)
#- 1 = Absolute, MSB as sign
#- 2 = High precision (only available for certain terminals)
epicsEnvSet("ECMC_EC_SDO_INDEX_OFFSET",  "0x2")
epicsEnvSet("ECMC_EC_SDO_SIZE",          "1")
epicsEnvSet("ECMC_EC_SDO_VALUE",         "2")  # High precision
ecmcConfigOrDie "Cfg.EcAddSdo(${ECMC_EC_SLAVE_NUM},${ECMC_EC_SDO_INDEX},$(ECMC_EC_SDO_INDEX_OFFSET),$(ECMC_EC_SDO_VALUE),$(ECMC_EC_SDO_SIZE))"

#- Connection technology:
#- 0 = Two wire
#- 1 = Three wire
#- 2 = Four wire
#- 0x80n0:1A
#- Default setting is different depending on which terminal.
epicsEnvSet("ECMC_EC_SDO_INDEX_OFFSET",  "0x1A")
epicsEnvSet("ECMC_EC_SDO_SIZE",          "2")
epicsEnvSet("ECMC_EC_SDO_VALUE",         "2")  # Four wire
ecmcConfigOrDie "Cfg.EcAddSdo(${ECMC_EC_SLAVE_NUM},${ECMC_EC_SDO_INDEX},$(ECMC_EC_SDO_INDEX_OFFSET),$(ECMC_EC_SDO_VALUE),$(ECMC_EC_SDO_SIZE))"

#- Calibration of supply lines (1/32 Ω):
#- Cable length = 1.5m
#- Wire area: 0.25mm²
#- Specific resistance of copper is 0.0175 Ω mm²/m
#- R=0.0175*1.5/0.25=0.1Ω
#- Settings = 0.1/(1/32)=3
epicsEnvSet("ECMC_EC_SDO_INDEX_OFFSET",  "0x1B")
epicsEnvSet("ECMC_EC_SDO_SIZE",          "2")
epicsEnvSet("ECMC_EC_SDO_VALUE",         "3")  # 0.1Ω
ecmcConfigOrDie "Cfg.EcAddSdo(${ECMC_EC_SLAVE_NUM},${ECMC_EC_SDO_INDEX},$(ECMC_EC_SDO_INDEX_OFFSET),$(ECMC_EC_SDO_VALUE),$(ECMC_EC_SDO_SIZE))"

#- Cleanup
epicsEnvUnset("ECMC_EC_SDO_INDEX_OFFSET")
epicsEnvUnset("ECMC_EC_SDO_SIZE")
epicsEnvUnset("ECMC_EC_SDO_VALUE")

