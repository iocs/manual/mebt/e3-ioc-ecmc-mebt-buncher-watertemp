require essioc
require ecmccfg, 8.0.0

epicsEnvSet("IOC" ,"MEBT-010:Ctrl-ECAT-004")

iocshLoad ${ecmccfg_DIR}startup.cmd, "ECMC_VER=8.0.2, MASTER_ID=1, NAMING=ESSnaming"

iocshLoad ${ecmccfg_DIR}addSlave.cmd, "HW_DESC=EK1100"
iocshLoad ${ecmccfg_DIR}addSlave.cmd, "HW_DESC=EL3202-0010"
iocshLoad $(E3_CMD_TOP)/hw/ecmcEL3202-0010-Sensor-chX.cmd, "ECMC_EC_SDO_INDEX=0x8000"
iocshLoad $(E3_CMD_TOP)/hw/ecmcEL3202-0010-Sensor-chX.cmd, "ECMC_EC_SDO_INDEX=0x8010"
iocshLoad ${ecmccfg_DIR}addSlave.cmd, "HW_DESC=EL3202-0010"
iocshLoad $(E3_CMD_TOP)/hw/ecmcEL3202-0010-Sensor-chX.cmd, "ECMC_EC_SDO_INDEX=0x8000"
iocshLoad $(E3_CMD_TOP)/hw/ecmcEL3202-0010-Sensor-chX.cmd, "ECMC_EC_SDO_INDEX=0x8010"
iocshLoad ${ecmccfg_DIR}addSlave.cmd, "HW_DESC=EL3202-0010"
iocshLoad $(E3_CMD_TOP)/hw/ecmcEL3202-0010-Sensor-chX.cmd, "ECMC_EC_SDO_INDEX=0x8000"
iocshLoad $(E3_CMD_TOP)/hw/ecmcEL3202-0010-Sensor-chX.cmd, "ECMC_EC_SDO_INDEX=0x8010"
iocshLoad ${ecmccfg_DIR}addSlave.cmd, "HW_DESC=EL3202-0010"
iocshLoad $(E3_CMD_TOP)/hw/ecmcEL3202-0010-Sensor-chX.cmd, "ECMC_EC_SDO_INDEX=0x8000"
iocshLoad $(E3_CMD_TOP)/hw/ecmcEL3202-0010-Sensor-chX.cmd, "ECMC_EC_SDO_INDEX=0x8010"
iocshLoad ${ecmccfg_DIR}addSlave.cmd, "HW_DESC=EL3202-0010"
iocshLoad $(E3_CMD_TOP)/hw/ecmcEL3202-0010-Sensor-chX.cmd, "ECMC_EC_SDO_INDEX=0x8000"
iocshLoad $(E3_CMD_TOP)/hw/ecmcEL3202-0010-Sensor-chX.cmd, "ECMC_EC_SDO_INDEX=0x8010"
iocshLoad ${ecmccfg_DIR}addSlave.cmd, "HW_DESC=EL1808"
iocshLoad ${ecmccfg_DIR}addSlave.cmd, "HW_DESC=EL2819"

# PLC file to set the interlock based on the inputs
iocshLoad $(ecmccfg_DIR)loadPLCFile.cmd, "PLC_ID=0, SAMPLE_RATE_MS=100,FILE=$(E3_CMD_TOP)/plc/ilock_buncher1.plc")
iocshLoad $(ecmccfg_DIR)loadPLCFile.cmd, "PLC_ID=1, SAMPLE_RATE_MS=100,FILE=$(E3_CMD_TOP)/plc/ilock_buncher2.plc")
iocshLoad $(ecmccfg_DIR)loadPLCFile.cmd, "PLC_ID=2, SAMPLE_RATE_MS=100,FILE=$(E3_CMD_TOP)/plc/ilock_buncher3.plc")

dbLoadRecords "${E3_CMD_TOP}/db/aliases.db" "IOC=${IOC}:"
set_pass1_restoreFile "${E3_CMD_TOP}/autosave/default_values.sav" "IOC=${IOC}:"
iocshLoad("$(essioc_DIR)/common_config.iocsh")

ecmcConfigOrDie "Cfg.EcApplyConfig(1)"
iocshLoad ($(ecmccfg_DIR)setAppMode.cmd)
